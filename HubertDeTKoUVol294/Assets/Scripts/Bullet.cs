﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IPooledObject
{
    private float Life = 4f;
    Rigidbody2D rb;
    private float _bulletSpeed = 24f;

    ObjectPooler objectPoolerScript;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        objectPoolerScript = ObjectPooler.FindObjectOfType(typeof(ObjectPooler)) as ObjectPooler;
    }

    // Update is called once per frame
    void Update()
    {
        CheckLife();
        CheckCollision();
    }

    public void OnObjectSpawn()
    {

        Life = Random.Range(1f, 4f)/4f;
        Start();
        rb.AddForce(transform.up * _bulletSpeed);
    }
    public void CheckLife()
    {
        if (Life <= 0)
        {
            if (!StaticNameController.HundredTowers)
            {
                objectPoolerScript.SpawnFromPool("Tower", transform.position, Quaternion.identity);
            }
            Destroy(gameObject);
        }
        Life -= Time.deltaTime;
    }
    public void CheckCollision()
    {

    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.collider == true)
        {
            if(coll.collider.tag == "Tower")
            {
                coll.collider.gameObject.SetActive(false);
                StaticNameController.TowerNumbers--;
                Destroy(gameObject);
            }
        }
    }
}
