﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour, IPooledObject
{
    private float _summonSickness = 0;

    private float _turnCD = 0.5f;
    private float _turnCDCurrent;

    private float _turnAngleMin = 15;
    private float _turnAngleMax = 45;

    private int _bulletNumber = 12;
    public bool _towerSleep = true;

    public GameObject firePoint;

    public SpriteRenderer spriteColor;
    public Color color;

    ObjectPooler objectPoolerScript;

    Rigidbody2D rigidbody2D;
    public void OnObjectSpawn()
    {
        StaticNameController.TowerNumbers++;
        if(StaticNameController.TowerNumbers == 100)
        {
            HundredTowers();
        }
        _summonSickness = 6f;
        Start();
        TurnWhite();
        
    }
    
    private void Start()
    {
        _turnCDCurrent = _turnCD;
        rigidbody2D = GetComponent<Rigidbody2D>();
        objectPoolerScript = ObjectPooler.FindObjectOfType(typeof(ObjectPooler)) as ObjectPooler;
        firePoint = transform.Find("FirePoint").gameObject;
        spriteColor = GetComponent<SpriteRenderer>();
    }
    void Update()
    {
        if(_summonSickness <= 0)
        {
            ActiveTowerTasks();
        }
        else
        {
            InactiveTowerTasks();
        }
    }
    public void ActiveTowerTasks()
    {
        if(_turnCDCurrent <= 0 && !_towerSleep)
        {
            TurnRed();
            float turnAngle = Random.Range(_turnAngleMin, _turnAngleMax);
            rigidbody2D.rotation += turnAngle;
            objectPoolerScript.SpawnFromPool("Bullet", firePoint.transform.position, transform.rotation);
            _bulletNumber--;
            if(_bulletNumber == 0)
            {
                _towerSleep = true;
                TurnWhite();
            }
            _turnCDCurrent = _turnCD;
        }
        else
        {
            _turnCDCurrent -= Time.deltaTime;
        }
    }
    public void InactiveTowerTasks()
    {
        _summonSickness -= Time.deltaTime;
        if(_summonSickness <= 0)
        {
            _towerSleep = false;
            TurnWhite();
        }
    }
    public void TurnRed()
    {
        color = new Color(1, 0, 0);
        spriteColor.color = color;
    }
    public void TurnWhite()
    {
        color = new Color(1, 1, 1);
        spriteColor.color = color;
    }
    public void HundredTowers()
    {
        StaticNameController.HundredTowers = true;
        /*
         //To nie działa oczywiście, ale taki miałem zamysł na ten przedostatni punkt
        
        foreach (GameObject go in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            go._bulletNumber = 12;
            go._towerSleep = false;

        }
        */
    }
}
