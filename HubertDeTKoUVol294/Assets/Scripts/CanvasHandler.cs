﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasHandler : MonoBehaviour
{
    public Text text;
    public int number;
    // Update is called once per frame
    void Update()
    {
        number = StaticNameController.TowerNumbers;
        text.text = "Number of towers: " + number.ToString();
    }
}
